<?php
namespace app\videos\api;
use app\one_api\api\UserInit;

use app\videos\server\Config as ConfigServer;

class Config extends UserInit
{

    public function initialize() 
    {
        $this->check_login = false;
        parent::initialize();
        if (!isset($this->apiKey) || empty($this->apiKey) || cache('apiKey') != $this->apiKey) {
            return $this->_error('非法请求', [], 710);
        }
        $this->ConfigServer = new ConfigServer();
    }

    /**
     * 获取解析地址
     *
     * @param [type] $data
     * @param [type] $user
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function getConfig()
    {
        $data= $this->params;
        $result = $this->ConfigServer->getConfig($data, $this->user);
        if (false === $result) {
            return $this->_error($this->ConfigServer->getError(),'',80001);
        }
        return $this->_success("成功", $result);
    }
    /**
     * 生成海报
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function createPoster()
    {
        $data= $this->params;
        $result = $this->ConfigServer->createPoster($data, $this->user);
        if (false === $result) {
            return $this->_error($this->ConfigServer->getError(),'',80002);
        }
        return $this->_success("成功", $result);
    }

    public function updateInfo()
    {
        $data= $this->params;
        $result = $this->ConfigServer->updateInfo($data, $this->user);
        if (false === $result) {
            return $this->_error($this->ConfigServer->getError(),'',80003);
        }
        return $this->_success("成功", $result);
    }

}