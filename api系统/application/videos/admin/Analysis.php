<?php
namespace app\videos\admin;
use app\system\admin\Admin;
use app\videos\model\Analysis as AnalysisModel;

class Analysis extends Admin
{
    protected $oneModel = 'Analysis';//模型名称[通用添加、修改专用]
    protected $oneTable = '';//表名称[通用添加、修改专用]
    protected $oneAddScene = '';//添加数据验证场景名
    protected $oneEditScene = '';//更新数据验证场景名

    public function initialize()
    {
        parent::initialize();
        $this->AnalysisModel = new AnalysisModel();
    }

    public function index()
    {
        if ($this->request->isAjax()) {
            $map    = $data = [];
            $data   = input();
            if (isset($data['source_url']) && !empty($data['source_url'])) {
                $map[] = ['source_url|source_id|source_title', 'eq', trim($data['source_url'])];
            }
            $page   = isset($data['page']) ? $data['page'] : 1;
            $limit  = isset($data['limit']) ? $data['limit'] : 15;

            $data = $this->AnalysisModel->getList($map, $page, $limit);
            return $this->success('获取成功', '', $data);
        }
        return $this->fetch();
    }
}