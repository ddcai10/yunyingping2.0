<?php
/**
 * 模块菜单
 * 字段说明
 * url 【链接地址】格式：attachment/控制器/方法，可填写完整外链[必须以http开头]
 * param 【扩展参数】格式：a=123&b=234555
 */
return [
    [
        'pid'           => 0,
        'is_menu'       => 0,
        'title'         => '附件管理',
        'icon'          => 'el-icon-folder',
        'module'        => 'attachment',
        'url'           => 'attachment',
        'param'         => '',
        'target'        => '_self',
        'sort'          => 100,
    ],
];