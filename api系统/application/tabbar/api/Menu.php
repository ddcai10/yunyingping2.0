<?php
namespace app\tabbar\api;
use app\one_api\api\ApiInit;
use app\tabbar\server\Tabbar as TabbarService;

class Menu extends ApiInit
{
    public function initialize() {
        parent::initialize();
        if (!isset($this->apiKey) || empty($this->apiKey) || cache('apiKey') != $this->apiKey) {
            return $this->_error('非法请求', [], 710);
        }
        $this->TabbarService = new TabbarService();
    }
    /**
     * 获取tabbar菜单
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function getMenus() {
        $data = $this->params;
        $result = $this->TabbarService->getMenus($data);
        if (false === $result) {
            return $this->_error($this->TabbarService->getError(),'',7000);
        }
        return $this->_success('获取成功',$result);
    }

}