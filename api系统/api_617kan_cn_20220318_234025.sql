-- MySQL dump 10.13  Distrib 5.7.34, for Win64 (x86_64)
--
-- Host: localhost    Database: api_617kan_cn
-- ------------------------------------------------------
-- Server version	5.7.34-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `one_ad`
--

DROP TABLE IF EXISTS `one_ad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_ad` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 启动页广告 2 首页广告 3 播放页弹窗广告 4 播放页底部广告 5 视频播放前广告 6 播放前激励广告',
  `title` varchar(255) DEFAULT '' COMMENT '广告标题',
  `desc` varchar(255) DEFAULT '' COMMENT '广告简介',
  `ad_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 自定义图片广告 2 自定义视频广告 3 小程序BANNER广告 4 小程序视频广告 5 小程序插屏广告 6 小程序格子广告 7 小程序视频贴片广告 8 小程序激励式广告 9 小程序原生模板广告',
  `img` varchar(255) DEFAULT '' COMMENT '图片地址',
  `v_url` varchar(255) NOT NULL DEFAULT '' COMMENT '视频地址',
  `url` varchar(255) DEFAULT '' COMMENT '跳转链接',
  `status` tinyint(1) DEFAULT '1' COMMENT '1 启用 0 禁用',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT '0' COMMENT '绑定会员组id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='广告管理';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_ad`
--

LOCK TABLES `one_ad` WRITE;
/*!40000 ALTER TABLE `one_ad` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_ad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_album`
--

DROP TABLE IF EXISTS `one_album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_album` (
  `album_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL DEFAULT '0' COMMENT '站点id',
  `site_name` varchar(255) NOT NULL DEFAULT '' COMMENT '站点名称',
  `album_name` varchar(50) NOT NULL DEFAULT '' COMMENT '相册,名称',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `cover` varchar(255) NOT NULL DEFAULT '' COMMENT '背景图',
  `desc` varchar(255) NOT NULL DEFAULT '' COMMENT '介绍',
  `is_default` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否默认',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT '0' COMMENT '删除时间',
  `num` int(11) NOT NULL DEFAULT '0' COMMENT '相册图片数',
  PRIMARY KEY (`album_id`) USING BTREE,
  KEY `IDX_sys_album_is_default` (`is_default`) USING BTREE,
  KEY `IDX_sys_album_site_id` (`site_id`) USING BTREE,
  KEY `IDX_sys_album_sort` (`sort`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='[系统] 上传附件分组';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_album`
--

LOCK TABLES `one_album` WRITE;
/*!40000 ALTER TABLE `one_album` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_album` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_album_pic`
--

DROP TABLE IF EXISTS `one_album_pic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_album_pic` (
  `pic_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pic_name` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `pic_path` varchar(255) NOT NULL DEFAULT '' COMMENT '路径',
  `pic_spec` varchar(255) NOT NULL DEFAULT '' COMMENT '规格',
  `pic_hash` varchar(64) NOT NULL DEFAULT '' COMMENT '文件hash值',
  `site_id` int(11) NOT NULL DEFAULT '0' COMMENT '站点id',
  `drive` varchar(50) NOT NULL DEFAULT '' COMMENT '驱动local qiniu等',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT '0' COMMENT '删除时间',
  `album_id` int(11) NOT NULL DEFAULT '0' COMMENT '相册id',
  `ext` varchar(10) NOT NULL DEFAULT '' COMMENT '后缀',
  PRIMARY KEY (`pic_id`) USING BTREE,
  KEY `IDX_sys_album_pic_site_id` (`site_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='[系统] 上传附件';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_album_pic`
--

LOCK TABLES `one_album_pic` WRITE;
/*!40000 ALTER TABLE `one_album_pic` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_album_pic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_attachment`
--

DROP TABLE IF EXISTS `one_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `savepath` varchar(255) NOT NULL DEFAULT '' COMMENT '保存目录',
  `md5` varchar(255) NOT NULL DEFAULT '' COMMENT '文件md5',
  `type` varchar(255) NOT NULL DEFAULT '' COMMENT '图片类型',
  `location` tinyint(1) NOT NULL DEFAULT '0' COMMENT '储存环境 0 本地 1 云端',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_attachment`
--

LOCK TABLES `one_attachment` WRITE;
/*!40000 ALTER TABLE `one_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_banner`
--

DROP TABLE IF EXISTS `one_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_banner` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '视频id',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT 'banner标题',
  `img` varchar(1000) NOT NULL DEFAULT '' COMMENT 'banner图片地址',
  `isad` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0  banner 1 广告 2 小程序广告',
  `ad_url` varchar(255) DEFAULT NULL COMMENT '广告链接',
  `type` varchar(255) NOT NULL DEFAULT 'image' COMMENT '展示类型 image video mpad',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1 启用 0 禁用',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='轮播图';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_banner`
--

LOCK TABLES `one_banner` WRITE;
/*!40000 ALTER TABLE `one_banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_feedback`
--

DROP TABLE IF EXISTS `one_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL DEFAULT '' COMMENT '留言类型',
  `content` text NOT NULL COMMENT '留言内容',
  `imgs` text NOT NULL COMMENT '留言图片',
  `contact` varchar(50) NOT NULL DEFAULT '' COMMENT '联系方式',
  `replay` varchar(255) NOT NULL DEFAULT '' COMMENT '处理反馈',
  `is_read` tinyint(3) NOT NULL DEFAULT '0' COMMENT '是否已读 0 未读 1 已读',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户反馈';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_feedback`
--

LOCK TABLES `one_feedback` WRITE;
/*!40000 ALTER TABLE `one_feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_msg`
--

DROP TABLE IF EXISTS `one_msg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_msg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `content` varchar(1500) NOT NULL DEFAULT '' COMMENT '内容',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1 正常 0 停用',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='公告';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_msg`
--

LOCK TABLES `one_msg` WRITE;
/*!40000 ALTER TABLE `one_msg` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_msg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_news`
--

DROP TABLE IF EXISTS `one_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '新闻标题',
  `img` varchar(255) NOT NULL DEFAULT '' COMMENT '缩略图',
  `desc` varchar(255) NOT NULL DEFAULT '' COMMENT '内容简介',
  `content` text NOT NULL COMMENT '详情',
  `status` tinyint(3) NOT NULL DEFAULT '1' COMMENT '1 启用 0 禁用',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='新闻专题';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_news`
--

LOCK TABLES `one_news` WRITE;
/*!40000 ALTER TABLE `one_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_one_api`
--

DROP TABLE IF EXISTS `one_one_api`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_one_api` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `secret` varchar(100) NOT NULL DEFAULT '' COMMENT '密钥',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0禁用 1启用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='API接口控制';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_one_api`
--

LOCK TABLES `one_one_api` WRITE;
/*!40000 ALTER TABLE `one_one_api` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_one_api` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_system_annex`
--

DROP TABLE IF EXISTS `one_system_annex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_system_annex` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '关联的数据ID',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '类型',
  `group` varchar(100) NOT NULL DEFAULT 'sys' COMMENT '文件分组',
  `file` varchar(255) NOT NULL DEFAULT '' COMMENT '上传文件',
  `hash` varchar(64) NOT NULL DEFAULT '' COMMENT '文件hash值',
  `size` decimal(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '附件大小KB',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '使用状态(0未使用，1已使用)',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `hash` (`hash`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='[系统] 上传附件';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_system_annex`
--

LOCK TABLES `one_system_annex` WRITE;
/*!40000 ALTER TABLE `one_system_annex` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_system_annex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_system_config`
--

DROP TABLE IF EXISTS `one_system_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_system_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为系统配置(1是，0否)',
  `group` varchar(20) NOT NULL DEFAULT 'base' COMMENT '分组',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '配置标题',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '配置名称，由英文字母和下划线组成',
  `value` text NOT NULL COMMENT '配置值',
  `type` varchar(20) NOT NULL DEFAULT 'input' COMMENT '配置类型()',
  `options` text NOT NULL COMMENT '配置项(选项名:选项值)',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '文件上传接口',
  `tips` varchar(255) NOT NULL DEFAULT '' COMMENT '配置提示',
  `sort` int(10) unsigned NOT NULL DEFAULT '100' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='[系统] 系统配置';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_system_config`
--

LOCK TABLES `one_system_config` WRITE;
/*!40000 ALTER TABLE `one_system_config` DISABLE KEYS */;
INSERT INTO `one_system_config` VALUES (1,1,'sys','扩展配置分组','config_group','commoncfg:全局配置\r\nvideo:视频配置\r\nucenter:个人中心\r\nupdate_app:版本\r\nplayad:激励广告\r\nwechat:小程序配置\r\nwxmptpl:微信小程序订阅模版\r\npopup:弹窗配置\ncodekey:卡密接口配置','array',' ','','请按如下格式填写：&lt;br&gt;键值:键名&lt;br&gt;键值:键名&lt;br&gt;&lt;span style=&quot;color:#f00&quot;&gt;键值只能为英文、数字、下划线&lt;/span&gt;',2,1,0,1637126723),(2,1,'base','网站域名','site_domain','http://domain.com','input','','','',2,1,0,0),(3,1,'upload','图片上传大小限制','upload_image_size','0','input','','','单位：KB，0表示不限制大小',3,1,0,0),(4,1,'upload','允许上传图片格式','upload_image_ext','jpg,png,gif,jpeg,ico','input','','','多个格式请用英文逗号（,）隔开',4,1,0,0),(5,1,'upload','缩略图裁剪方式','thumb_type','2','select','1:等比例缩放\r\n2:缩放后填充\r\n3:居中裁剪\r\n4:左上角裁剪\r\n5:右下角裁剪\r\n6:固定尺寸缩放\r\n','','',5,1,0,0),(6,1,'upload','图片水印开关','image_watermark','0','switch','0:关闭\r\n1:开启','','',6,1,0,0),(7,1,'upload','图片水印图','image_watermark_pic','','image','','','',7,1,0,0),(8,1,'upload','图片水印透明度','image_watermark_opacity','50','input','','','可设置值为0~100，数字越小，透明度越高',8,1,0,0),(9,1,'upload','图片水印图位置','image_watermark_location','9','select','7:左下角\r\n1:左上角\r\n4:左居中\r\n9:右下角\r\n3:右上角\r\n6:右居中\r\n2:上居中\r\n8:下居中\r\n5:居中','','',9,1,0,0),(10,1,'upload','文件上传大小限制','upload_file_size','0','input','','','单位：KB，0表示不限制大小',1,1,0,0),(11,1,'upload','允许上传文件格式','upload_file_ext','doc,docx,xls,xlsx,ppt,pptx,pdf,wps,txt,rar,zip','input','','','多个格式请用英文逗号（,）隔开',2,1,0,0),(12,1,'upload','文字水印开关','text_watermark','0','switch','0:关闭\r\n1:开启','','',10,1,0,0),(13,1,'upload','文字水印内容','text_watermark_content','','input','','','',11,1,0,0),(14,1,'upload','文字水印字体','text_watermark_font','','file','','','不上传将使用系统默认字体',12,1,0,0),(15,1,'upload','文字水印字体大小','text_watermark_size','20','input','','','单位：px(像素)',13,1,0,0),(16,1,'upload','文字水印颜色','text_watermark_color','#000000','input','','','文字水印颜色，格式:#000000',14,1,0,0),(17,1,'upload','文字水印位置','text_watermark_location','7','select','7:左下角\r\n1:左上角\r\n4:左居中\r\n9:右下角\r\n3:右上角\r\n6:右居中\r\n2:上居中\r\n8:下居中\r\n5:居中','','',11,1,0,0),(18,1,'upload','缩略图尺寸','thumb_size','','input','','','为空则不生成，生成 500x500 的缩略图，则填写 500x500，多个规格填写参考 300x300;500x500;800x800',4,1,0,0),(19,1,'sys','开发模式','app_debug','1','switch','0:关闭\r\n1:开启','','&lt;strong class=&quot;red&quot;&gt;生产环境下一定要关闭此配置&lt;/strong&gt;',3,1,0,1636294648),(20,1,'sys','页面Trace','app_trace','0','switch','0:关闭\r\n1:开启','','&lt;strong class=&quot;red&quot;&gt;生产环境下一定要关闭此配置&lt;/strong&gt;',4,1,0,1607662815),(21,1,'databases','备份目录','backup_path','./backup/database/','input','','','数据库备份路径,路径必须以 / 结尾',0,1,0,0),(22,1,'databases','备份分卷大小','part_size','20971521','input','','','用于限制压缩后的分卷最大长度。单位：B；建议设置20M',0,1,0,0),(23,1,'databases','备份压缩开关','compress','1','switch','','','压缩备份文件需要PHP环境支持gzopen,gzwrite函数',0,1,0,0),(24,1,'databases','备份压缩级别','compress_level','1','radio','最低:1\r\n一般:4\r\n最高:9','','数据库备份文件的压缩级别，该配置在开启压缩时生效',0,1,0,0),(25,1,'base','网站状态','site_status','1','switch','0:关闭\r\n1:开启','','站点关闭后将不能访问，后台可正常登录',1,1,0,0),(26,1,'sys','后台管理路径','admin_path','admin.php','input','','','必须以.php为后缀',1,1,0,1647617376),(27,1,'base','网站标题','site_title','网站标题','input','','','网站标题是体现一个网站的主旨，要做到主题突出、标题简洁、连贯等特点，建议不超过28个字',6,1,0,0),(28,1,'base','网站关键词','site_keywords','网站关键词 ','input','','','网页内容所包含的核心搜索关键词，多个关键字请用英文逗号&quot;,&quot;分隔',7,1,0,0),(29,1,'base','网站描述','site_description','','textarea','','','网页的描述信息，搜索引擎采纳后，作为搜索结果中的页面摘要显示，建议不超过80个字',8,1,0,0),(30,1,'base','ICP备案信息','site_icp','','input','','','请填写ICP备案号，用于展示在网站底部，ICP备案官网：&lt;a href=&quot;http://www.miibeian.gov.cn&quot; target=&quot;_blank&quot;&gt;http://www.miibeian.gov.cn&lt;/a&gt;',9,1,0,0),(31,1,'base','站点统计代码','site_statis','','textarea','','','第三方流量统计代码，前台调用时请先用 htmlspecialchars_decode函数转义输出',10,1,0,0),(32,1,'base','网站名称','site_name','网站名称 ','input','','','将显示在浏览器窗口标题等位置',3,1,0,0),(33,1,'base','网站LOGO','site_logo','','image','','','网站LOGO图片',4,1,0,1607662841),(34,1,'base','手机网站','wap_site_status','0','switch','0:关闭\r\n1:开启','','如果有手机网站，请设置为开启状态，否则只显示PC网站',2,1,0,1607575916),(35,1,'base','手机网站域名','wap_domain','http://m.domain.com','input','','','手机访问将自动跳转至此域名，示例：http://m.domain.com',2,1,0,0),(36,1,'sys','后台白名单验证','admin_whitelist_verify','0','switch','0:禁用\r\n1:启用','','禁用后不存在的菜单节点将不在提示',7,1,0,0),(37,1,'sys','系统日志保留','system_log_retention','30','input','','','单位天，系统将自动清除 ? 天前的系统日志',8,1,0,0),(38,1,'upload','上传驱动','upload_driver','local','select','local:本地上传','','资源上传驱动设置',0,1,0,0),(41,0,'video','api解析接口','analysis_api_url','','input','','','解析接口地址',100,1,1613707955,1647617644),(50,0,'video','播放器封面图','video_poster','','image','','','',100,1,1613819001,1647617644),(52,0,'commoncfg','开屏广告','show_ad','0','switch','0:关闭\n1:开启','','',100,1,1613819256,1636216400),(55,0,'commoncfg','全局广告开关','ad_switch','1','switch','0:关闭\n1:开启','','此开关控制首页、分类列表页、个人中心页及所有内页的广告',100,1,1613823149,1631164516),(56,0,'commoncfg','播放前弹窗广告','player_show_ad','1','switch','0:关闭\n1:开启','','',100,1,1613823305,1643027514),(57,0,'commoncfg','欺骗式关闭按钮','ad_close','0','switch','0:关闭\n1:开启','','广告上的关闭按钮，若开启，点击关闭即为点击广告，后退后广告窗口自动关闭；若关闭，点击后直接关闭广告弹窗',100,1,1613823417,1613823835),(58,0,'commoncfg','播放页底部广告','player_bottom_ad','0','switch','0:关闭\n1:开启','','',100,1,1613823479,1638668302),(59,0,'commoncfg','视频播放前广告','show_video_ad','0','switch','0:关闭\n1:开启','','此配置项打开后，在加载视频前会先展示小程序视频广告',100,1,1613823534,1630517549),(60,0,'commoncfg','小程序审核','show_xx','1','switch','0:关闭\n1:开启','','开启后小程序将无法播放视频，只可查看视频介绍',100,1,1613823642,1638668115),(61,0,'commoncfg','会员播放等级限制','member_group_switch','1','switch','0:关闭\n1:开启','','开启后，将根据设定的会员等级进行判断是否给予观看权限，该配置优先级高于小程序审核开关（如果开了小程序审核开关，但是会员满足设定的等级，那么观看将不受影响）',100,1,1613823686,1630957390),(62,0,'commoncfg','会员播放等级ID','member_group_limit','3','input','','','可以正常观看的会员等级，设定3级的话，3级以下的会员将无法观看。<br>注：当设置为最低等级id的时候，代表着所有人都可以正常观看',100,1,1613823727,1631039281),(63,0,'popup','首页弹窗开关','show_popup','1','switch','0:关闭\n1:开启','','是否展示首页引导关注弹窗',100,1,1613823752,1636294341),(64,0,'popup','弹窗图片','popup_img','','image','','','配置引导关注的公众号二维码',102,1,1613823773,1647617680),(65,0,'popup','图片文字','popup_title','','input','','','首页引导关注提示语',103,1,1613823792,1647617680),(66,0,'ucenter','关于应用','about_app','1、本APP仅做交流学习使用。','textarea','','','',100,1,1613830274,1613830274),(67,0,'ucenter','服务条款','punch','1.617kan APP不提供任何视听上传服务，所有内容均来自视频分享站点所提供的公开引用资源，所有视频及图文版权均归原作者及其网站所有。617kan不存储，不修改界面内容。视频数据流也不经由本公司服务器中转或存储。本站将竭尽所能注明资源来源，但由于互联网转载的不可预性，无法确认所有内容的版权所有人。若原作者对本站所载视频作品版权的归属存有异议，请联系QQ:723875993,我们将在第一时间予以删除。\n\n      2.任何存在于617kan APP上的视频、图文资料均系他人制作或提供，仅为个人观点，不代表617kan APP网站立场。您可能从这些视频、图文资料.上获得资讯，617kan APP对其合法性概不负责，亦不承担任何法律责任。\n\n      3.617kan APP对网络视频的聚合和分类，是根据用户观看习惯做的浏览引导，这样可以方便用户更快捷的找到相应的视频，本软件并没有对电视频道做编辑和整理。\n\n      4.您应该对浏览使617kan APP一切服务自行承担风险。我们不做任何形式的保证:不保证站内搜索结果满足您的要求，不保证网站服务不中断，不保证视频及图文资源的安全性、正确性、及时性、合法性。因网络状况、通讯线路、第三方网站等任何原因而导致您不能正常使用617kan APP，617kan APP不承担任何法律责任。\n\n      5.617kan APP尊重并保护所有使用617kan APP用户的个人隐私权，您注册的用户名等个人资料，非经您亲自许可或根据相关法律、法规的强制性规定，617kan APP不会主动地泄露给第三方。\n\n      6.任何单位或个人认为通过617kan APP提供的内容可能涉嫌侵犯其信息网络传播权，应该及时向617kan APP提出书面权利通知，并提供身份证明、权属证明及详细侵权情况证明。617kan APP在收到.上述法律文件后，将会依法尽快断开相关链接内容。\n\n      7.617kan APP一切资源仅为学习交流娱乐所用，请在下载后24小时内删除，未经版权许可，任何单位或个人不得将本站内容或服务用于商业目的。','textarea','','','',100,1,1613830291,1613830291),(68,0,'update_app','IOS版本','ios_version','1.0.0','input','','','当前最新版本的app版本号，和客户端的globalConfigs.js里面配置的iosVersion如果相等则不跟新，如果不相等且atOnce为true时，会提示更新',100,1,1613830317,1613830317),(69,0,'update_app','IOS下载地址','ios_download_url','-','input','','','此url为APP STORE的下载地址',100,1,1613830340,1613830340),(70,0,'update_app','IOS更新日志','ios_log','1、更新\n2、更新','textarea','','','更新日志',100,1,1613830356,1613830356),(71,0,'update_app','立即更新','ios_atonce','0','switch','0:关闭\n1:开启','','如果有更新，是否立即提示更新，关闭时不更新，开启时会提示更新，此提示可用于app上架审核期间控制不提示更新',100,1,1613830394,1613830394),(72,0,'update_app','安卓版本号','android_version','1.1.4','input','','','当前最新版本的app版本号，和客户端的globalConfigs.js里面配置的iosVersion如果相等则不跟新，如果不相等且atOnce为true时，会提示更新',100,1,1613830410,1631983029),(73,0,'update_app','安卓下载地址','android_download_url','','input','','','安卓文件的链接地址，例如：http://xxx/download/ethy.apk',100,1,1613830444,1613830444),(74,0,'update_app','安卓更新日志','android_log','1、修复弹幕发送bug\n2、优化首页加载','textarea','','','安卓的更新日志内容',100,1,1613830460,1613830460),(75,0,'update_app','立即更新','android_atonce','1','switch','0:关闭\n1:开启','','如果有更新，是否立即提示更新，关闭时不更新，开启会提示更新，此提示可用于app上架审核期间控制不提示更新',100,1,1613830493,1631863533),(76,0,'playad','播放前激励广告','play_start_ad','0','switch','0:关闭\n1:开启','','视频播放前的激励广告；如果打开该配置，播放页弹窗广告和视频播放前广告都将自动失效',100,1,1613830529,1613830529),(77,0,'playad','激励广告必看','should_play_end','0','switch','0:关闭\n1:开启','','开启后用户必须看完激励广告才可以观看视频，否则不可观看视频',100,1,1613830565,1636255497),(78,0,'playad','激励广告限次展示','play_ad_switch','1','switch','0:关闭\n1:开启','','开启后，激励广告将根据设置的次数展示，达到设定次数后不再展示激励广告',100,1,1613830594,1636214947),(79,0,'playad','激励广告出现次数','play_ad_num','100','input','','','一天内展示激励广告的次数',100,1,1613830621,1643032675),(80,0,'playad','激励广告弹窗标题','play_ad_title','广告时间','input','','','',100,1,1613830634,1636214947),(81,0,'playad','激励广告弹窗内容','play_ad_text','点击广告解锁完整视频，每天只需点击2次哦','textarea','','','',100,1,1613830646,1636796204),(82,0,'commoncfg','登录页动态背景','login_bg_video','','input','','','填写视频地址，mp4等视频格式，建议大小不超过3M，若超出3M可自行上CDN地址',100,1,1620573741,1647617634),(83,0,'commoncfg','登录页按钮上部文字','login_bg_text','狩猎时刻到了！','input','','','最多20个字',100,1,1620576990,1631201490),(87,0,'video','视频源名称','source_name','','textarea','','','自定义播放源名称，格式：标识:名称；如：youku:优酷',100,1,1621238512,1647617644),(88,0,'commoncfg','壁纸下载按钮文字','wallpaper_button_text','点我观看广告即可免费下载','input','','','下载壁纸按钮的文字，审核小程序广告组件时需要改成“观看广告免费下载”',100,1,1621242622,1621396687),(91,0,'video','首页推荐栏目id','recommend_id','1','input','','','首页推荐栏目分类id，在cms的视频分类里查看',100,1,1628498186,1628498250),(92,0,'video','首页电影栏目id','movie_id','1','input','','','首页电影栏目分类id，在cms的视频分类里查看',100,1,1628498241,1628498391),(93,0,'video','首页电视剧栏目id','tv_id','2','input','','','首页电视剧目分类id，在cms的视频分类里查看',100,1,1628498276,1628498530),(94,0,'video','首页动漫栏目id','acg_id','4','input','','','首页动漫栏目分类id，在cms的视频分类里查看',100,1,1628498301,1628498364),(95,0,'video','首页综艺栏目id','variety_id','3','input','','','首页综艺栏目分类id，在cms的视频分类里查看',100,1,1628498330,1628498375),(96,0,'video','播放页复制地址','copy_url','0','switch','0:关闭\n1:开启','','',100,1,1626877778,1630859509),(97,0,'commoncfg','强制观看广告下载壁纸','wallpaper_ad_lock','1','switch','0:关闭\n1:开启','','是否必须看广告才能下载',100,1,1627131064,1637840832),(98,0,'wechat','开启自定义TARBAR','diy_tarbar','0','switch','0:关闭\n1:开启','','开启后底部tarbar将不受小程序对tarbar的最大数量限制，可随意跳转页面，更加灵活，但此模式将严重降低小程序及APP的页面展示性能，有利有弊，酌情使用！另外为保证用户体验，自定义模式下已限制最大tarbar数量为7个',100,1,1628101784,1632472550),(99,0,'wechat','Appid','appid','','input','','','',100,1,1620747124,1647617660),(100,0,'wechat','Secret','secret','','input','','','',100,1,1620747137,1647617660),(101,0,'wechat','CMS域名','cms_host','','input','','','填写cms所在服务器域名，用来显示图片',100,1,1621133961,1621134013),(104,0,'playad','播放中插播激励广告','show_ad_half','1','switch','0:关闭\n1:开启','','视频播放时是否弹出激励广告',102,1,1621330982,1630734920),(105,0,'playad','视频播放多久插播广告','show_ad_time','600','input','','','单位：秒；播放多久后插播激励广告，需开启播放中插播激励广告开关',120,1,1621331102,1636251786),(114,0,'commoncfg','首页弹窗图片','kefu_qrcode','','image','','','',99,1,1628735554,1647617634),(115,0,'wechat','底部菜单突起','tarbar_up','0','switch','0:关闭\n1:开启','','底部菜单是否需要突起',100,1,1629044036,1629552854),(116,0,'wechat','播放页引导按钮','wxwp_switch','0','switch','0:关闭\n1:开启','','是否开启小程序内跳转到公众号文章，需配置公众号绑定小程序后才可使用；开启后按钮将在用户登录后但无播放权限时自动展示',100,1,1629172267,1641213801),(117,0,'wechat','公众号文章链接','wxmp_url','','input','','','小程序内跳转到公众号文章，需配置公众号绑定小程序后才可使用',103,1,1629172294,1631037074),(118,0,'wechat','公众号按钮文字','wxmp_btn_text','关注公众号查看','input','','','',105,1,1629172529,1631037115),(119,0,'commoncfg','兑换码关键字','code_key','你好','input','','','发放二维码时检测的关键字，若检测到该关键字，应用将自动读取兑换码，引导用户兑换',100,1,1629177862,1639470653),(120,0,'wechat','自定义全局分享封面图','share_img','','image','','','除播放页外分享封面图',110,1,1629222897,1629483884),(121,0,'wechat','自定义全局分享标题','share_title','云影评','input','','','除播放页外分享标题',110,1,1629223087,1629461552),(122,0,'video','解析白名单','analysis_white_list','','textarea','','','不参与解析的地址关键字，填写后触发关键字的不走解析，直接播放；可以填url地址后缀或者域名关键字',100,1,1629358497,1647617644),(123,0,'wxmptpl','订阅模板id','template_id','','input','','','所需下发的订阅模板id，在微信小程序后台-订阅消息-公共模板库内搜索“电视剧”添加',100,1,1629653426,1647617670),(124,0,'wxmptpl','模板卡片的跳转页面','page','/pages/index/index?froms=1','input','','','其余不熟悉不要修改，只需要在最后填视频id即可！点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。',100,1,1629653568,1636252368),(125,0,'wxmptpl','模版内容','tpl_content','thing1:最新资讯\ntime6:2021年12月\nthing5:最新资讯','textarea','','','该值在微信小程序后台-订阅消息-我的模版-详情-详细内容中获取；例如：{{thing1.DATA}}，thing1则为key值，直接复制过来，不可修改，DATA则为对应的内容，可以修改，格式为key:DATA，中间用英文逗号分割！多条内容按照看到的从上到下的顺序换行分割',100,1,1629655875,1647617670),(126,0,'wechat','默认搜索热词','hot_search_key','','input','','','搜索框内默认搜索热词，可填写近期热门电视剧、电影',110,1,1629713460,1647617660),(127,0,'wxmptpl','是否启用该功能','wxmp_tpl_switch','1','switch','0:关闭\n1:开启','','关闭后将不再提醒用户订阅',100,1,1629721279,1636252368),(128,0,'wxmptpl','提醒订阅间隔（分）','wxmp_tpl_Interval','5','input','','','提示用户订阅的弹窗间隔',100,1,1629721553,1629721553),(129,0,'wechat','官方客服按钮','kefu_switch','1','switch','0:关闭\n1:开启','','打开后客户按钮将改为小程序官方客服按钮，关闭则为自己上传的二维码',110,1,1629780703,1629798019),(130,0,'playad','激励广告总开关','play_ad_control','0','switch','0:关闭\n1:开启','','开启此开关后下面的设置才可生效',90,1,1629834926,1637492329),(132,0,'wxmptpl','用户定向订阅备注内容','tpl_remark','您订阅的内容更新啦，快来看看吧','input','','','用户订阅某一个视频时，备注提示内容',100,1,1630173196,1631304690),(133,0,'commoncfg','无广告会员等级id','no_ad_limit','5','input','','','设置后满足此会员组的用户将不会再展示广告。0为不开启',100,1,1630394541,1643027588),(134,0,'wechat','显示兑换码入口','gift_switch','0','switch','0:关闭\n1:开启','','',110,1,1632373799,1640530373),(135,0,'popup','弹窗类型','popup_type','1','radio','0:弹窗图片\n1:弹窗文字\n3:对话框\n5:分享转发','','首页弹窗的展示类型',101,1,1632465687,1640679633),(138,0,'popup','文字内容','popup_msg','','textarea','','','弹窗文字类型的文字内容',104,1,1632479204,1647617680),(139,0,'popup','对话框标题','popup_dialog_title','提示','input','','','对话框类型窗口标题',105,1,1632479296,1632479296),(140,0,'popup','对话框内容','popup_dialog_content','这是相关内容','input','','','对话框类型的提示内容',106,1,1632479349,1632479349),(141,0,'popup','对话框确定按钮','popup_dialog_confirm_text','跳转','input','','','对话框类型的确定按钮文字',107,1,1632479393,1632819139),(142,0,'popup','对话框取消按钮','popup_dialog_close_text','取消','input','','','对话框类型的取消按钮文字',108,1,1632479435,1632479435),(143,0,'popup','对话框确定按钮跳转类型','popup_dialog_confirm_type','5','radio','0:无操作\n1:分享小程序\n2:跳转到其他小程序\n3:跳转到外部链接\n4:跳转本小程序指定页\n5:预览弹窗图片','','对话框类型点击确定按钮后的操作',109,1,1632479547,1640594346),(144,0,'popup','其他小程序appid','popup_dialog_confirm_appid',' ','input','','','若确定按钮类型为跳转到其他小程序，这里需要填写其他小程序的appid，若小程序地址为小程序链接，此项可不填写，具体查看：https://developers.weixin.qq.com/miniprogram/dev/api/navigate/wx.navigateToMiniProgram.html',112,1,1632479638,1632483494),(145,0,'popup','跳转地址','popup_dialog_confirm_page','/packageA/pages/user/child/giftCode','input','','','若确定按钮类型为跳转到其他小程序，这里需要填写要跳转到其他小程序的页面地址；具体规则可查看：https://developers.weixin.qq.com/miniprogram/dev/api/navigate/wx.navigateToMiniProgram.html',111,1,1632479699,1636214591),(148,0,'commoncfg','悬浮按钮跳转类型','drag_btn_type','4','radio','1:本小程序\n2:其他小程序\n3:外部链接\n4:图片弹窗','','外部链接：小程序只可跳转公众号地址，其余只可APP使用；图片弹窗需配置“首页弹窗图片”',110,1,1632575526,1641211878),(149,0,'commoncfg','外部小程序appid','drag_btn_appid','appid','input','','','悬浮按钮跳转的外部小程序appid',111,1,1632575599,1632575599),(150,0,'commoncfg','跳转地址','drag_btn_page','/packageA/pages/user/ucenter','input','','','本小程序：填写小程序页面地址；其他小程序：若填写小程序链接，则无需填写小程序appid；外部链接：小程序只可跳转绑定的公众号页面，其余只有APP可用',111,1,1632575669,1640945362),(151,0,'codekey','单人获取频次','get_codekey_limit','0','input','','','一个人一天内可获取的次数',97,1,1637126809,1640530388),(152,0,'codekey','卡密有效期','codekey_exp','2','input','','','单位：分；获取的卡密有效时长，超时后将重新生成一个新的',98,1,1637126885,1638600963),(153,0,'codekey','裂变规则说明','top_public','<div style=\"display: flex;flex-direction: row;		justify-content: space-around;margin-bottom:5px;\">\n<span style=\"font-size: 18px;font-weight: bold;\">分享链接，解锁无广告\n</span>\n</div>\n<p style=\"font-size:14px;\">\n<span style=\"font-size:16px;\">规则：</span>\n<br/>\n成功推荐满${S1}人后将自动升级为10天无广告会员；<br/>\n成功推荐满${S2}人后将自动升级为20天无广告会员；<br/>\n成功推荐满${S3}人后将自动升级为永久无广告会员；\n</p>\n<br/>\n<p style=\"font-size:14px;\">\n<span style=\"font-size:16px;\">规则说明：</span>\n<br/>\n登录小程序，使用兑换码激活会员后，点击小程序内的头像复制小程序id，填写到下面输入框内，点击“获取推广链接”生成专属推广链接，点击“复制链接”后分享给朋友，朋友打开链接获取并复制兑换码，登录小程序后成功使用兑换码激活会员，即可累计1次。\n</p>','textarea','','','',100,1,1637221815,1638011719),(154,0,'codekey','裂变阶梯规则（人）','rules','5-8-10','input','','','累计人数阶梯，中间以-分割，从左到右依次递增；如：10-20-30，则为满10人...满20人...满30人...',100,1,1637223682,1638011719),(155,0,'codekey','裂变阶梯规则（会员组）','rules_group','5-6-4','input','','','此会员组id与上面的人数对应，如：人：10-20-30，会员组：2-3-4，则为满10人升级到会员组id：2，满20人升级到会员组id：3，满30人升级到会员组id：4',100,1,1637224692,1637645712),(156,0,'codekey','开启裂变活动','share_switch','0','switch','0:关闭\n1:开启','','是否开启该活动',99,1,1637643717,1638438517),(157,0,'commoncfg','自保模式','save_model','0','switch','0:关闭\n1:开启','','开启后小程序将无法再播放，只能复制播放地址到外部浏览器观看',103,1,1640936092,1642232625),(158,0,'commoncfg','播放器地址','save_player','','input','','','开启自保模式后，用户复制到的播放地址直链前的网页播放器地址',104,1,1640940746,1647617634),(159,0,'wechat','播放页引导按钮类型','wxmp_btn_type','1','radio','0:跳转公众号文章\n1:弹窗图片','','',100,1,1641212210,1641213771),(160,0,'wechat','播放页引导图片','wxmp_img','','image','','','',100,1,1641212397,1647617660),(161,0,'commoncfg','悬浮按钮图片','drag_btn_img','','image','','','',111,1,1632575423,1647617634),(162,0,'commoncfg','页面悬浮按钮','drag_btn_switch','0','switch','0:关闭\n1:开启','','',109,1,1641214755,1641218710);
/*!40000 ALTER TABLE `one_system_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_system_hook`
--

DROP TABLE IF EXISTS `one_system_hook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_system_hook` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '系统插件',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '钩子名称',
  `source` varchar(50) NOT NULL DEFAULT '' COMMENT '钩子来源[plugins.插件名，module.模块名]',
  `intro` varchar(200) NOT NULL DEFAULT '' COMMENT '钩子简介',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='[系统] 钩子表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_system_hook`
--

LOCK TABLES `one_system_hook` WRITE;
/*!40000 ALTER TABLE `one_system_hook` DISABLE KEYS */;
INSERT INTO `one_system_hook` VALUES (1,1,'system_admin_index','系统','后台首页',1,0,0),(2,1,'system_admin_tips','系统','后台所有页面提示',1,0,0),(3,1,'system_annex_upload','系统','附件上传钩子，可扩展上传到第三方存储',1,0,0),(4,0,'user_login','module.user','会员登陆成功之后的动作',1,1636210066,1636210066),(5,0,'user_register','module.user','会员注册成功后的动作',1,1636210066,1636210066),(6,0,'user_delete','module.user','会员删除成功后的动作',1,1636210066,1636210066);
/*!40000 ALTER TABLE `one_system_hook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_system_hook_plugins`
--

DROP TABLE IF EXISTS `one_system_hook_plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_system_hook_plugins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hook` varchar(32) NOT NULL DEFAULT '' COMMENT '钩子id',
  `plugins` varchar(32) NOT NULL DEFAULT '' COMMENT '插件标识',
  `sort` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='[系统] 钩子-插件对应表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_system_hook_plugins`
--

LOCK TABLES `one_system_hook_plugins` WRITE;
/*!40000 ALTER TABLE `one_system_hook_plugins` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_system_hook_plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_system_language`
--

DROP TABLE IF EXISTS `one_system_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_system_language` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '语言包名称',
  `code` varchar(20) NOT NULL DEFAULT '' COMMENT '编码',
  `locale` varchar(255) NOT NULL DEFAULT '' COMMENT '本地浏览器语言编码',
  `icon` varchar(30) NOT NULL DEFAULT '' COMMENT '图标',
  `pack` varchar(100) NOT NULL DEFAULT '' COMMENT '上传的语言包',
  `sort` tinyint(2) unsigned NOT NULL DEFAULT '100',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='[系统] 语言包';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_system_language`
--

LOCK TABLES `one_system_language` WRITE;
/*!40000 ALTER TABLE `one_system_language` DISABLE KEYS */;
INSERT INTO `one_system_language` VALUES (1,'简体中文','zh-cn','zh-CN,zh-CN.UTF-8,zh-cn','','1',1,1);
/*!40000 ALTER TABLE `one_system_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_system_log`
--

DROP TABLE IF EXISTS `one_system_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_system_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `url` text,
  `param` text,
  `remark` varchar(255) NOT NULL DEFAULT '',
  `count` int(10) unsigned NOT NULL DEFAULT '1',
  `ip` varchar(128) NOT NULL DEFAULT '',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='[系统] 操作日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_system_log`
--

LOCK TABLES `one_system_log` WRITE;
/*!40000 ALTER TABLE `one_system_log` DISABLE KEYS */;
INSERT INTO `one_system_log` VALUES (1,1,'个人信息','/admin.php/system/user/info.html','[]','浏览数据',2,'39.130.123.55',1647617983,1647618013),(2,1,'个人信息','/admin.php?s=system%2Fuser%2Finfo','{\"username\":\"admin\",\"nick\":\"\\u8d85\\u7ea7\\u7ba1\\u7406\\u5458\",\"password\":\"123123\",\"password_confirm\":\"123123\",\"email\":\"\",\"mobile\":\"\",\"_csrf\":\"\"}','保存数据',2,'39.130.123.55',1647617989,1647618021),(3,1,'首页','/admin.php?s=system%2Findex%2Findex','[]','浏览数据',2,'39.130.123.55',1647617990,1647618022),(4,1,'首页','/admin.php/system/index/index.html','[]','浏览数据',1,'39.130.123.55',1647618009,1647618009);
/*!40000 ALTER TABLE `one_system_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_system_menu`
--

DROP TABLE IF EXISTS `one_system_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_system_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(5) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID(快捷菜单专用)',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `module` varchar(20) NOT NULL DEFAULT '' COMMENT '模块名或插件名，插件名格式:plugins.插件名',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '菜单标题',
  `icon` varchar(80) NOT NULL DEFAULT '' COMMENT '菜单图标',
  `url` varchar(200) NOT NULL DEFAULT '' COMMENT '链接地址(模块/控制器/方法)',
  `param` varchar(200) NOT NULL DEFAULT '' COMMENT '扩展参数',
  `target` varchar(20) NOT NULL DEFAULT '_self' COMMENT '打开方式(_blank,_self)',
  `sort` int(10) unsigned NOT NULL DEFAULT '100' COMMENT '排序',
  `debug` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '开发模式可见',
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为系统菜单，系统菜单不可删除',
  `nav` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否为菜单显示，1显示0不显示',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态1显示，0隐藏',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='[系统] 管理菜单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_system_menu`
--

LOCK TABLES `one_system_menu` WRITE;
/*!40000 ALTER TABLE `one_system_menu` DISABLE KEYS */;
INSERT INTO `one_system_menu` VALUES (1,0,0,'system','首页','','system/index/index','','_self',100,0,1,1,1,1607490932,0),(2,0,0,'system','系统','','system/system/index','','_self',100,0,1,1,1,1607490932,0),(3,0,1,'system','个人信息','','system/user/info','','_self',100,0,1,0,1,1607490932,0),(4,0,1,'system','清空缓存','','system/index/clear','','_self',100,0,1,0,1,1607490932,0),(5,0,1,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(6,0,1,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(7,0,1,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(8,0,2,'system','系统设置','','system/system/index','','_self',100,0,1,1,1,1607490932,0),(9,0,2,'system','系统扩展','','system/extend/index','','_self',100,1,1,1,1,1607490932,0),(10,0,2,'system','系统管理员','','system/user/index','','_self',100,0,1,1,1,1607490932,0),(11,0,2,'system','数据库管理','','system/database/index','','_self',100,0,1,1,1,1607490932,0),(12,0,2,'system','系统日志','','system/log/index','','_self',100,0,1,1,1,1607490932,0),(13,0,2,'system','配置管理','','system/config/index','','_self',100,1,1,1,1,1607490932,0),(14,0,8,'system','基础配置','','system/system/index','group=base','_self',100,0,1,0,1,1607490932,0),(15,0,8,'system','系统配置','','system/system/index','group=sys','_self',100,0,1,0,1,1607490932,0),(16,0,8,'system','上传配置','','system/system/index','group=upload','_self',100,0,1,0,1,1607490932,0),(17,0,8,'system','开发配置','','system/system/index','group=develop','_self',100,0,1,0,1,1607490932,0),(18,0,8,'system','数据库配置','','system/system/index','group=databases','_self',100,0,1,0,1,1607490932,0),(19,0,9,'system','本地模块','','system/module/index','','_self',100,0,1,1,1,1607490932,0),(20,0,9,'system','模块钩子','','system/hook/index','','_self',100,0,1,1,1,1607490932,0),(21,0,8,'system','视频配置','','system/system/index','group=video','_self',100,0,1,0,1,1607490932,0),(22,0,2,'system','系统更新','','system/upgrade/index','','_self',100,0,1,1,1,1607490932,0),(23,0,9,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(24,0,62,'system','添加管理员','','system/user/adduser','','_self',100,0,1,0,1,1607490932,0),(25,0,62,'system','修改管理员','','system/user/edituser','','_self',100,0,1,0,1,1607490932,0),(26,0,62,'system','删除管理员','','system/user/deluser','','_self',100,0,1,0,1,1607490932,0),(27,0,62,'system','状态设置','','system/user/status','','_self',100,0,1,0,1,1607490932,0),(28,0,62,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(29,0,11,'system','备份数据库','','system/database/export','','_self',100,0,1,0,1,1607490932,0),(30,0,11,'system','恢复数据库','','system/database/import','','_self',100,0,1,0,1,1607490932,0),(31,0,11,'system','优化数据库','','system/database/optimize','','_self',100,0,1,0,1,1607490932,0),(32,0,11,'system','删除备份','','system/database/del','','_self',100,0,1,0,1,1607490932,0),(33,0,11,'system','修复数据库','','system/database/repair','','_self',100,0,1,0,1,1607490932,0),(34,0,12,'system','清空日志','','system/log/clear','','_self',100,0,1,0,1,1607490932,0),(35,0,12,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(36,0,12,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(37,0,12,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(38,0,12,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(39,0,19,'system','安装模块','','system/module/install','','_self',100,0,1,0,1,1607490932,0),(40,0,19,'system','卸载模块','','system/module/uninstall','','_self',100,0,1,0,1,1607490932,0),(41,0,19,'system','状态设置','','system/module/status','','_self',100,0,1,0,1,1607490932,0),(42,0,19,'system','设置默认模块','','system/module/setdefault','','_self',100,0,1,0,1,1607490932,0),(43,0,19,'system','删除模块','','system/module/del','','_self',100,0,1,0,1,1607490932,0),(44,0,19,'system','重载模块','','system/module/reinstall','','_self',100,0,1,0,1,1607490932,0),(50,0,20,'system','添加钩子','','system/hook/add','','_self',100,0,1,0,1,1607490932,0),(51,0,20,'system','修改钩子','','system/hook/edit','','_self',100,0,1,0,1,1607490932,0),(52,0,20,'system','删除钩子','','system/hook/del','','_self',100,0,1,0,1,1607490932,0),(53,0,20,'system','状态设置','','system/hook/status','','_self',100,0,1,0,1,1607490932,0),(54,0,20,'system','插件排序','','system/hook/sort','','_self',100,0,1,0,1,1607490932,0),(55,0,13,'system','添加配置','','system/config/add','','_self',100,0,1,0,1,1490315067,0),(56,0,13,'system','修改配置','','system/config/edit','','_self',100,0,1,0,1,1490315067,0),(57,0,13,'system','删除配置','','system/config/del','','_self',100,0,1,0,1,1490315067,0),(58,0,13,'system','状态设置','','system/config/status','','_self',100,0,1,0,1,1490315067,0),(59,0,13,'system','排序设置','','system/config/sort','','_self',100,0,1,0,1,1490315067,0),(60,0,13,'system','添加分组','','system/config/addgroup','','_self',100,0,1,0,1,1607490932,0),(61,0,13,'system','删除分组','','system/config/delgroup','','_self',100,0,1,0,1,1607490932,0),(62,0,10,'system','管理用户','','system/user/index','','_self',100,0,1,0,1,1607490932,0),(63,0,10,'system','管理角色','','system/user/role','','_self',100,0,1,0,1,1607490932,0),(64,0,63,'system','添加角色','','system/user/addrole','','_self',100,0,1,1,1,1490315067,0),(65,0,63,'system','修改角色','','system/user/editrole','','_self',100,0,1,1,1,1490315067,0),(66,0,63,'system','删除角色','','system/user/delrole','','_self',100,0,1,1,1,1490315067,0),(67,0,63,'system','状态设置','','system/user/statusRole','','_self',100,0,1,1,1,1490315067,0),(68,0,8,'system','全局配置','','system/system/index','group=commoncfg','_self',99,0,1,0,1,1607490932,0),(69,0,8,'system','个人中心','','system/system/index','group=ucenter','_self',101,0,1,0,1,1607490932,0),(70,0,8,'system','版本','','system/system/index','group=update_app','_self',101,0,1,0,1,1607490932,0),(71,0,8,'system','激励广告','','system/system/index','group=playad','_self',101,0,1,0,1,1607490932,0),(72,0,8,'system','小程序配置','','system/system/index','group=wechat','_self',101,0,1,0,1,1607490932,0),(73,0,8,'system','微信小程序订阅模版','','system/system/index','group=wxmptpl','_self',101,0,1,0,1,1607490932,0),(74,0,8,'system','弹窗配置','','system/system/index','group=popup','_self',101,0,1,0,1,1607490932,0),(75,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(76,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(77,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(78,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(79,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(80,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(81,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(82,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(83,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(84,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(85,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(86,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(87,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(88,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(89,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(90,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(91,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(92,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(93,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(94,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(95,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(96,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(97,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(98,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(99,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(100,0,0,'system','预留占位','','','','_self',100,0,1,0,0,1607490932,0),(101,0,2,'system','附件管理','','system/album/index','','_self',100,0,1,1,1,1607490932,0),(102,0,2,'system','驱动管理','','system/album.drive/index','','_self',100,0,1,0,1,1607490932,0),(103,0,2,'system','回收管理','','system/album/recycle','','_self',100,0,1,0,1,1607490932,0),(104,0,0,'banner','广告管理','mdi mdi-alpha-a-circle-outline','banner','','_self',102,0,0,1,1,1636209975,1636209975),(105,0,104,'banner','轮播图','typcn typcn-chart-bar-outline','banner/banner/index','','_self',0,0,0,1,1,1636209975,1636209975),(106,0,105,'banner','编辑','','banner/banner/edit','','_self',0,0,0,0,1,1636209975,1636209975),(107,0,104,'banner','广告位','typcn typcn-chart-bar-outline','banner/ad/index','','_self',0,0,0,1,1,1636209975,1636209975),(108,0,107,'banner','编辑','','banner/ad/edit','','_self',0,0,0,0,1,1636209975,1636209975),(109,0,0,'codekey','卡密管理','el-icon-postcard','codekey','','_self',100,0,0,1,1,1636209977,1636209977),(110,0,109,'codekey','卡密列表','fa fa-credit-card','codekey/index/index','','_self',0,0,0,1,1,1636209977,1636209977),(111,0,110,'codekey','编辑','fa fa-credit-card','codekey/index/createCodekey','','_self',0,0,0,0,1,1636209977,1636209977),(112,0,0,'news','新闻公告','el-icon-postcard','news','','_self',100,0,0,1,1,1636209978,1636209978),(113,0,112,'news','新闻列表','aicon ai-caidan','news/index/index','','_self',0,0,0,1,1,1636209978,1636209978),(114,0,113,'news','增加','aicon ai-tianjia','news/index/add','','_self',0,0,0,0,1,1636209978,1636209978),(115,0,113,'news','编辑','aicon ai-error','news/index/edit','','_self',0,0,0,0,1,1636209978,1636209978),(116,0,113,'news','删除','aicon ai-jinyong','news/index/del','','_self',0,0,0,0,1,1636209978,1636209978),(117,0,112,'news','公告','aicon ai-caidan','news/msg/index','','_self',0,0,0,1,1,1636209978,1636209978),(118,0,117,'news','增加','aicon ai-tianjia','news/msg/add','','_self',0,0,0,0,1,1636209978,1636209978),(119,0,117,'news','编辑','aicon ai-error','news/msg/edit','','_self',0,0,0,0,1,1636209978,1636209978),(120,0,117,'news','删除','aicon ai-jinyong','news/msg/del','','_self',0,0,0,0,1,1636209978,1636209978),(121,0,0,'one_api','API授权','mdi mdi-artstation','one_api/index/index','','_self',100,0,0,1,1,1636209981,1636209981),(122,0,121,'one_api','添加','','one_api/index/add','','_self',100,0,0,0,1,1636209981,1636209981),(123,0,121,'one_api','编辑','','one_api/index/edit','','_self',100,0,0,0,1,1636209981,1636209981),(124,0,121,'one_api','删除','','one_api/index/del','','_self',100,0,0,0,1,1636209981,1636209981),(125,0,121,'one_api','改变状态','','one_api/index/status','','_self',100,0,0,0,1,1636209981,1636209981),(126,0,0,'tabbar','底部菜单','mdi mdi-dropbox','tabbar','','_self',100,0,0,1,1,1636209983,1636209983),(127,0,126,'tabbar','菜单列表','fa fa-credit-card','tabbar/index/index','','_self',0,0,0,1,1,1636209983,1636209983),(128,0,127,'tabbar','编辑','fa fa-credit-card','tabbar/index/edit','','_self',0,0,0,0,1,1636209983,1636209983),(129,0,126,'tabbar','菜单类型列表','fa fa-credit-card','tabbar/type/index','','_self',0,0,0,1,1,1636209983,1636209983),(130,0,129,'tabbar','编辑','fa fa-credit-card','tabbar/type/edit','','_self',0,0,0,0,1,1636209983,1636209983),(131,0,0,'user','会员','el-icon-s-custom','user','','_self',100,0,0,1,1,1636210066,1636210066),(132,0,131,'user','会员列表','aicon ai-huiyuanliebiao','user/index/index','','_self',0,0,1,1,1,1636210066,1636210066),(133,0,132,'user','添加会员','','user/index/add','','_self',1,0,0,0,1,1636210066,1636210066),(134,0,132,'user','修改会员','','user/index/edit','','_self',2,0,0,0,1,1636210066,1636210066),(135,0,132,'user','删除会员','','user/index/del','','_self',3,0,0,0,1,1636210066,1636210066),(136,0,132,'user','状态设置','','user/index/status','','_self',4,0,0,0,1,1636210066,1636210066),(137,0,132,'user','会员选择','','user/index/pop','','_self',5,0,0,0,1,1636210066,1636210066),(138,0,132,'user','重置密码','','user/index/resetPwd','','_self',6,0,0,0,1,1636210066,1636210066),(139,0,131,'user','会员分组','aicon ai-huiyuandengji','user/group/index','','_self',2,0,1,1,1,1636210066,1636210066),(140,0,139,'user','添加分组','','user/group/add','','_self',0,0,0,0,1,1636210066,1636210066),(141,0,139,'user','修改分组','','user/group/edit','','_self',0,0,0,0,1,1636210066,1636210066),(142,0,139,'user','删除分组','','user/group/del','','_self',0,0,0,0,1,1636210066,1636210066),(143,0,139,'user','设置默认分组','','user/group/setDefault','','_self',0,0,0,0,1,1636210066,1636210066),(144,0,131,'user','反馈留言','aicon ai-error','user/feedback/index','','_self',3,0,0,1,1,1636210066,1636210066),(145,0,131,'user','行为记录','aicon ai-error','user/behavior/index','','_self',4,0,0,1,1,1636210066,1636210066),(146,0,0,'videos','视频管理','el-icon-film','videos','','_self',102,0,0,1,1,1636210068,1636210068),(147,0,146,'videos','解析索引','typcn typcn-chart-bar-outline','videos/analysis/index','','_self',0,0,0,1,1,1636210068,1636210068),(148,0,147,'videos','编辑','','videos/analysis/edit','','_self',0,0,0,0,1,1636210068,1636210068),(149,0,146,'videos','报错反馈','typcn typcn-chart-bar-outline','videos/report/index','','_self',0,0,0,1,1,1636210068,1636210068),(150,0,146,'videos','评论管理','typcn typcn-chart-bar-outline','videos/replay/index','','_self',0,0,0,1,1,1636210068,1636210068);
/*!40000 ALTER TABLE `one_system_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_system_menu_lang`
--

DROP TABLE IF EXISTS `one_system_menu_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_system_menu_lang` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(120) NOT NULL DEFAULT '' COMMENT '标题',
  `lang` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '语言包',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='[系统] 管理菜单语言包';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_system_menu_lang`
--

LOCK TABLES `one_system_menu_lang` WRITE;
/*!40000 ALTER TABLE `one_system_menu_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_system_menu_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_system_module`
--

DROP TABLE IF EXISTS `one_system_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_system_module` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '系统模块',
  `name` varchar(50) NOT NULL COMMENT '模块名(英文)',
  `identifier` varchar(100) NOT NULL COMMENT '模块标识(模块名(字母).开发者标识.module)',
  `title` varchar(50) NOT NULL COMMENT '模块标题',
  `intro` varchar(255) NOT NULL COMMENT '模块简介',
  `author` varchar(100) NOT NULL COMMENT '作者',
  `icon` varchar(80) NOT NULL DEFAULT 'aicon ai-mokuaiguanli' COMMENT '图标',
  `version` varchar(20) NOT NULL COMMENT '版本号',
  `url` varchar(255) NOT NULL COMMENT '链接',
  `sort` int(5) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0未安装，1未启用，2已启用',
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '默认模块(只能有一个)',
  `config` text NOT NULL COMMENT '配置',
  `app_id` varchar(30) NOT NULL DEFAULT '0' COMMENT '应用市场ID(0本地)',
  `app_keys` varchar(200) DEFAULT '' COMMENT '应用秘钥',
  `theme` varchar(50) NOT NULL DEFAULT 'default' COMMENT '主题模板',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE,
  UNIQUE KEY `identifier` (`identifier`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='[系统] 模块';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_system_module`
--

LOCK TABLES `one_system_module` WRITE;
/*!40000 ALTER TABLE `one_system_module` DISABLE KEYS */;
INSERT INTO `one_system_module` VALUES (2,0,'attachment','attachment.617.module','附件管理','网站附件管理','617','/static/attachment/attachment.png','1.0.0','',0,2,0,'','0','','default',1636209971,1636209971),(3,0,'banner','banner.617.module','广告管理','小程序流量主广告、轮播图、启动图管理','617','/static/banner/banner.png','1.0.0','',0,2,0,'','0','','default',1636209971,1636209971),(4,0,'codekey','codekey.617.module','卡密管理','会员卡密管理','617','/static/codekey/codekey.png','1.0.0','www.qdapi.com',0,2,0,'','0','','default',1636209971,1636209971),(5,0,'news','news.617.module','新闻公告','发布相关文章及公告','617','/static/news/news.png','1.0.0','https://www.qdapi.com',0,2,0,'','0','','default',1636209971,1636209971),(6,0,'one_api','one','分层API','扩展分层模型,对外提供RESTful接口,带基本验证','one','/static/one_api/one_api.png','1.0.0','https://www.ouenyi.cn/',0,2,0,'','0','','default',1636209971,1636209971),(7,0,'tabbar','2afb0a03c4ea1f51be41a44d2d573a5c','底部菜单','管理底部tabbar菜单','617','/static/tabbar/tabbar.png','1.0.0','https://www.ouenyi.cn/',0,2,0,'','0','','default',1636209971,1636209971),(8,0,'user','user.617.module','会员','站点会员管理','617','/static/user/user.png','1.0.2','http://www.qdapi.com',0,2,0,'','0','','default',1636209971,1636209971),(9,0,'videos','videos.617.module','视频管理','视频管理模块，需配合cms系统使用','617','/static/videos/videos.png','1.0.0','',0,2,0,'','0','','default',1636209971,1636209971),(10,0,'wallpaper','258651ebe579e2d9aa1504828ffe89c9','壁纸','','617','/static/wallpaper/wallpaper.png','1.0.0','https://www.ouenyi.cn/',0,2,0,'','0','','default',1636209971,1636209971);
/*!40000 ALTER TABLE `one_system_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_system_plugins`
--

DROP TABLE IF EXISTS `one_system_plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_system_plugins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '插件名称(英文)',
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT '插件标题',
  `icon` varchar(64) NOT NULL DEFAULT '' COMMENT '图标',
  `intro` varchar(500) NOT NULL COMMENT '插件简介',
  `author` varchar(32) NOT NULL DEFAULT '' COMMENT '作者',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '作者主页',
  `version` varchar(16) NOT NULL DEFAULT '' COMMENT '版本号',
  `identifier` varchar(64) NOT NULL DEFAULT '' COMMENT '插件唯一标识符',
  `config` text NOT NULL COMMENT '插件配置',
  `app_id` varchar(30) NOT NULL DEFAULT '0' COMMENT '来源(0本地)',
  `app_keys` varchar(200) NOT NULL DEFAULT '' COMMENT '应用秘钥',
  `sort` int(10) unsigned NOT NULL DEFAULT '100' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='[系统] 插件表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_system_plugins`
--

LOCK TABLES `one_system_plugins` WRITE;
/*!40000 ALTER TABLE `one_system_plugins` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_system_plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_system_role`
--

DROP TABLE IF EXISTS `one_system_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_system_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '角色名称',
  `intro` varchar(200) NOT NULL COMMENT '角色简介',
  `auth` text NOT NULL COMMENT '角色权限',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='[系统] 管理角色';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_system_role`
--

LOCK TABLES `one_system_role` WRITE;
/*!40000 ALTER TABLE `one_system_role` DISABLE KEYS */;
INSERT INTO `one_system_role` VALUES (1,'超级管理员','','',1,1606883052,1606883052);
/*!40000 ALTER TABLE `one_system_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_system_upload`
--

DROP TABLE IF EXISTS `one_system_upload`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_system_upload` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL DEFAULT '' COMMENT '上传平台code',
  `abbrev` varchar(20) DEFAULT '' COMMENT '上传平台简称',
  `title` varchar(50) NOT NULL COMMENT '上传平台标题',
  `intro` varchar(255) NOT NULL COMMENT '上传平台简介',
  `config` text NOT NULL COMMENT '配置',
  `applies` varchar(10) NOT NULL DEFAULT 'pc' COMMENT '适用环境(pc,wap,wechat,app)',
  `sort` int(10) unsigned NOT NULL DEFAULT '100' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态(0停用，1启用)',
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '默认',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='[upload] 上传平台';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_system_upload`
--

LOCK TABLES `one_system_upload` WRITE;
/*!40000 ALTER TABLE `one_system_upload` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_system_upload` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_system_user`
--

DROP TABLE IF EXISTS `one_system_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_system_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(64) NOT NULL DEFAULT '',
  `nick` varchar(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `mobile` varchar(11) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `auth` text NOT NULL COMMENT '权限',
  `iframe` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0默认，1框架',
  `theme` varchar(50) NOT NULL DEFAULT 'default' COMMENT '主题',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `last_login_ip` varchar(128) NOT NULL DEFAULT '' COMMENT '最后登陆IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后登陆时间',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='[系统] 管理用户';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_system_user`
--

LOCK TABLES `one_system_user` WRITE;
/*!40000 ALTER TABLE `one_system_user` DISABLE KEYS */;
INSERT INTO `one_system_user` VALUES (1,'admin','$2y$10$FQjoUAMpeexjQi4w225mCu3m9rA050RXemuu.RQVFlyH2VvRJu4K.','超级管理员','','','',0,'default',1,'39.130.123.55',1647618009,1636209912,1647618022);
/*!40000 ALTER TABLE `one_system_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_system_user_role`
--

DROP TABLE IF EXISTS `one_system_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_system_user_role` (
  `user_id` int(11) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='管理员角色索引';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_system_user_role`
--

LOCK TABLES `one_system_user_role` WRITE;
/*!40000 ALTER TABLE `one_system_user_role` DISABLE KEYS */;
INSERT INTO `one_system_user_role` VALUES (1,1);
/*!40000 ALTER TABLE `one_system_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_tabbar`
--

DROP TABLE IF EXISTS `one_tabbar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_tabbar` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL DEFAULT '0' COMMENT '导航类型',
  `nav_name` varchar(255) NOT NULL DEFAULT '' COMMENT '导航名称',
  `nav_alias` varchar(255) NOT NULL DEFAULT '' COMMENT '别名',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT '导航图标',
  `active_icon` varchar(255) NOT NULL DEFAULT '' COMMENT '导航选中图标',
  `sort` tinyint(3) NOT NULL DEFAULT '100' COMMENT '排序',
  `jump_type` tinyint(3) NOT NULL DEFAULT '1' COMMENT '跳转类型 1 本小程序 2 其他小程序',
  `jump_url` varchar(255) NOT NULL DEFAULT '' COMMENT '跳转地址',
  `appid` varchar(255) NOT NULL DEFAULT '' COMMENT '其他小程序appid',
  `mp_page` varchar(255) NOT NULL DEFAULT '' COMMENT '其他小程序页面地址',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) DEFAULT '0',
  `is_default` tinyint(3) NOT NULL DEFAULT '0' COMMENT '是否默认',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='底部菜单列表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_tabbar`
--

LOCK TABLES `one_tabbar` WRITE;
/*!40000 ALTER TABLE `one_tabbar` DISABLE KEYS */;
INSERT INTO `one_tabbar` VALUES (1,1,'主页','Home','/upload/sys/image/e3/b70dab18457a9791175edf70b104ea.png','/upload/sys/image/a9/3bba0dd1415729c321e782a7322fa4.png',1,1,'/pages/index/index','','',1,1620287087,1636211214,1),(2,1,'搜索','Search','/upload/sys/image/6c/b33c9ecffbab1dbdaa523a80177669.png','/upload/sys/image/46/540a216ded32faf7565053bcaf2a8e.png',3,1,'/pages/search/index','','',1,1620287113,1636211223,0),(3,1,'分类','Category','/upload/sys/image/b2/0227c5190fbaae54ffad921d8a116b.png','/upload/sys/image/31/45ca1a81f99d71f3189c8ee9007b77.png',2,1,'/pages/cate/index','','',1,1620287128,1621612034,0),(4,1,'壁纸','Wallpaper','/upload/sys/image/85/21c1794d48d95dc1970845855f66b9.png','/upload/sys/image/a0/9340db150022365751a5d6ae1e88ac.png',4,1,'/pages/wallpaper/index','','',1,1620287141,1621612046,0),(5,1,'我的','Mine','/upload/sys/image/3d/8ba1d62941c394810c373b37617fe5.png','/upload/sys/image/e8/fbeca4720515d75d55eac975703184.png',5,1,'/pages/user/index','','',1,1620287141,1621612053,0);
/*!40000 ALTER TABLE `one_tabbar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_tabbar_type`
--

DROP TABLE IF EXISTS `one_tabbar_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_tabbar_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '类型名',
  `status` tinyint(3) NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='底部菜单分类';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_tabbar_type`
--

LOCK TABLES `one_tabbar_type` WRITE;
/*!40000 ALTER TABLE `one_tabbar_type` DISABLE KEYS */;
INSERT INTO `one_tabbar_type` VALUES (1,'底部菜单',1,1620287027,1620287027);
/*!40000 ALTER TABLE `one_tabbar_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_user`
--

DROP TABLE IF EXISTS `one_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级id',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员分组ID',
  `nick` varchar(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `username` varchar(30) NOT NULL DEFAULT '' COMMENT '用户名',
  `mobile` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT '手机号',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `password` varchar(128) NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(16) NOT NULL DEFAULT '' COMMENT '密码混淆',
  `money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '可用金额',
  `frozen_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '冻结金额',
  `income` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '收入统计',
  `expend` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '开支统计',
  `exper` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '经验值',
  `integral` int(10) unsigned NOT NULL DEFAULT '100' COMMENT '积分',
  `frozen_integral` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '冻结积分',
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '性别(1男，0女)',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `wxmp_openid` varchar(255) NOT NULL DEFAULT '' COMMENT 'mp_openid',
  `has_bind` tinyint(3) NOT NULL DEFAULT '0' COMMENT '是否已绑定用户名 0 待绑定 1 已绑定',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注信息',
  `last_login_ip` varchar(128) NOT NULL DEFAULT '' COMMENT '最后登陆IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后登陆时间',
  `login_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登陆次数',
  `expire_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '到期时间(0永久)',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态(0禁用，1正常)',
  `ctime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='[系统] 会员表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_user`
--

LOCK TABLES `one_user` WRITE;
/*!40000 ALTER TABLE `one_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_user_behavior`
--

DROP TABLE IF EXISTS `one_user_behavior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_user_behavior` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `type` tinyint(3) NOT NULL DEFAULT '1' COMMENT '类型 1 截图',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '其他信息',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '状态 0 未处理 1 已处理',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='用户行为记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_user_behavior`
--

LOCK TABLES `one_user_behavior` WRITE;
/*!40000 ALTER TABLE `one_user_behavior` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_user_behavior` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_user_group`
--

DROP TABLE IF EXISTS `one_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_user_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL COMMENT '等级名称',
  `min_exper` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最小经验值',
  `max_exper` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最大经验值',
  `discount` int(2) unsigned NOT NULL DEFAULT '100' COMMENT '折扣率(%)',
  `intro` varchar(255) NOT NULL COMMENT '等级简介',
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '默认等级',
  `expire` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员有效期(天)',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ctime` int(10) unsigned NOT NULL DEFAULT '0',
  `mtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='[系统] 会员等级';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_user_group`
--

LOCK TABLES `one_user_group` WRITE;
/*!40000 ALTER TABLE `one_user_group` DISABLE KEYS */;
INSERT INTO `one_user_group` VALUES (1,'散仙',0,0,100,'',1,0,1,1545105600,1545105600),(2,'体验',0,0,100,'',0,1,1,1545105600,1545105600),(3,'元婴',0,0,100,'',0,0,1,1545105600,1545105600),(4,'神人',0,0,100,'',0,0,1,1545105600,1545105600),(5,'10天无广告',0,0,100,'',0,10,1,0,0),(6,'20天无广告',0,0,100,'',0,20,1,0,0);
/*!40000 ALTER TABLE `one_user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_user_group_codekey`
--

DROP TABLE IF EXISTS `one_user_group_codekey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_user_group_codekey` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '绑定用户',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '对应会员组',
  `codekey` varchar(32) NOT NULL DEFAULT '' COMMENT '卡密',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '使用状态 0 未使用 1 已使用',
  `exp_time` int(11) NOT NULL DEFAULT '0' COMMENT '有效期',
  `code_type` tinyint(3) NOT NULL DEFAULT '1' COMMENT '类型 1 一次性 2 可复用',
  `use_num` int(11) NOT NULL DEFAULT '1' COMMENT '可用次数',
  `used_num` int(11) NOT NULL DEFAULT '0' COMMENT '已用次数',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) NOT NULL DEFAULT '' COMMENT '前端接口ip地址',
  `device_id` varchar(20) NOT NULL DEFAULT '' COMMENT '设备 id',
  `uuid` varchar(50) NOT NULL DEFAULT '' COMMENT '用户设备唯一标识 ip + id + 型号 md5',
  `model` varchar(50) NOT NULL DEFAULT '' COMMENT '用户设备型号',
  `from_user` varchar(50) NOT NULL DEFAULT '' COMMENT '来源openid',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `codekey` (`codekey`,`from_user`,`uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='会员卡密';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_user_group_codekey`
--

LOCK TABLES `one_user_group_codekey` WRITE;
/*!40000 ALTER TABLE `one_user_group_codekey` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_user_group_codekey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_user_like`
--

DROP TABLE IF EXISTS `one_user_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_user_like` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT '0' COMMENT '会员id',
  `vid` text NOT NULL COMMENT '视频id',
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户收藏';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_user_like`
--

LOCK TABLES `one_user_like` WRITE;
/*!40000 ALTER TABLE `one_user_like` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_user_like` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_user_token`
--

DROP TABLE IF EXISTS `one_user_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_user_token` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `token` varchar(200) NOT NULL DEFAULT '' COMMENT '用户token',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  `expire_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '到期时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户token';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_user_token`
--

LOCK TABLES `one_user_token` WRITE;
/*!40000 ALTER TABLE `one_user_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_user_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_user_wallet`
--

DROP TABLE IF EXISTS `one_user_wallet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_user_wallet` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `money` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '返利',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='推广获利（用户钱包）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_user_wallet`
--

LOCK TABLES `one_user_wallet` WRITE;
/*!40000 ALTER TABLE `one_user_wallet` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_user_wallet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_user_wallet_log`
--

DROP TABLE IF EXISTS `one_user_wallet_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_user_wallet_log` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `uid` mediumint(8) unsigned NOT NULL,
  `value` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变动金额',
  `msg` text NOT NULL,
  `dateline` int(10) unsigned NOT NULL DEFAULT '0',
  `type` varchar(20) NOT NULL DEFAULT '',
  `admin_id` int(10) unsigned DEFAULT '0',
  `money_detail` tinytext NOT NULL COMMENT '余额明细',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='财务变动记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_user_wallet_log`
--

LOCK TABLES `one_user_wallet_log` WRITE;
/*!40000 ALTER TABLE `one_user_wallet_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_user_wallet_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_video_analysis`
--

DROP TABLE IF EXISTS `one_video_analysis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_video_analysis` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `unique` varchar(32) NOT NULL DEFAULT '' COMMENT '唯一标识（视频id+源播放地址）',
  `source_url` varchar(500) NOT NULL DEFAULT '' COMMENT '源地址',
  `format_url` varchar(3000) NOT NULL DEFAULT '' COMMENT '解析地址',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `unique` (`unique`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='视频解析索引';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_video_analysis`
--

LOCK TABLES `one_video_analysis` WRITE;
/*!40000 ALTER TABLE `one_video_analysis` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_video_analysis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_video_replay`
--

DROP TABLE IF EXISTS `one_video_replay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_video_replay` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `vod_id` int(11) NOT NULL DEFAULT '0' COMMENT '视频id',
  `vod_name` varchar(255) NOT NULL DEFAULT '' COMMENT '视频名称',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '评论内容',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '用户头像',
  `nickname` varchar(255) NOT NULL DEFAULT '' COMMENT '用户昵称',
  `status` tinyint(3) NOT NULL DEFAULT '1' COMMENT '状态 0 待审 1 通过',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='视频评论';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_video_replay`
--

LOCK TABLES `one_video_replay` WRITE;
/*!40000 ALTER TABLE `one_video_replay` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_video_replay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_video_report`
--

DROP TABLE IF EXISTS `one_video_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_video_report` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vod_id` int(11) NOT NULL DEFAULT '0' COMMENT '视频id',
  `vod_play_from` varchar(11) NOT NULL DEFAULT '' COMMENT '视频源',
  `vod_name` varchar(255) NOT NULL DEFAULT '' COMMENT '视频名称',
  `vod_index` varchar(255) NOT NULL DEFAULT '' COMMENT '视频播放序列（第几集）',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注信息',
  `number` int(11) NOT NULL DEFAULT '1' COMMENT '反馈次数',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '是否已处理 0 未处理 1 已处理',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='视频反馈';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_video_report`
--

LOCK TABLES `one_video_report` WRITE;
/*!40000 ALTER TABLE `one_video_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_video_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_video_subscribe`
--

DROP TABLE IF EXISTS `one_video_subscribe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_video_subscribe` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `vod_id` int(11) NOT NULL DEFAULT '0' COMMENT '视频id',
  `vod_title` varchar(255) NOT NULL DEFAULT '' COMMENT '视频标题',
  `vod_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `vod_score` varchar(255) NOT NULL DEFAULT '5' COMMENT '评分',
  `vod_year` varchar(255) NOT NULL DEFAULT '' COMMENT '首播时间',
  `vod_actor` varchar(255) NOT NULL DEFAULT '' COMMENT '主演',
  `status` tinyint(3) NOT NULL COMMENT '状态 0 未推送 1 已推送',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_video_subscribe`
--

LOCK TABLES `one_video_subscribe` WRITE;
/*!40000 ALTER TABLE `one_video_subscribe` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_video_subscribe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_videos_danmu`
--

DROP TABLE IF EXISTS `one_videos_danmu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_videos_danmu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `vid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '视频id',
  `index` tinyint(3) NOT NULL DEFAULT '0' COMMENT '剧集索引',
  `text` varchar(150) NOT NULL DEFAULT '' COMMENT '弹幕内容',
  `color` varchar(150) NOT NULL DEFAULT '' COMMENT '弹幕颜色',
  `time` float(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '弹幕时间',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='视频弹幕';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_videos_danmu`
--

LOCK TABLES `one_videos_danmu` WRITE;
/*!40000 ALTER TABLE `one_videos_danmu` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_videos_danmu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_wallpaper`
--

DROP TABLE IF EXISTS `one_wallpaper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_wallpaper` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='壁纸';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_wallpaper`
--

LOCK TABLES `one_wallpaper` WRITE;
/*!40000 ALTER TABLE `one_wallpaper` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_wallpaper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'api_617kan_cn'
--

--
-- Dumping routines for database 'api_617kan_cn'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-18 23:40:26
