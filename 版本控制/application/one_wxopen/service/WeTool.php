<?php
namespace app\one_wxopen\service;

use Exception;

include_once(env('app_path').'one_wxopen'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'WeTool'.DIRECTORY_SEPARATOR.'include.php');

class WeTool
{
    /**
     * 接口类型模式
     * @var string
     */
    private static $type = 'WeMini';

    /**
     * 实例微信对象
     * @param string $name
     * @param string $appid 授权公众号APPID
     * @param string $type 将要获取SDK类型
     * @return mixed
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public static function instance($name, $appid = '', $type = null)
    {
        $file = env('app_path').'one_wxopen'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'wxopen.ini';
        if (file_exists($file) === true) {
            $_config = @parse_ini_file($file, true);
        } else {
            $_config = [];
        }
        $config = [
            'component_appid'          => $_config['open_appid'],
            'component_token'          => $_config['open_token'],
            'component_appsecret'      => $_config['open_secret'],
            'component_encodingaeskey' => $_config['open_key'],
        ];
        //注册授权 AccessToken 处理
        $config['GetAccessTokenCallback'] = function ($authorizer_appid) use ($config) {
            $open = new \WeOpen\Service($config);
            $path= env('app_path').implode(DIRECTORY_SEPARATOR,['one_wxopen','mpdata']).DIRECTORY_SEPARATOR;
            $file = $path.$authorizer_appid.'.ini';
            if (file_exists($file) === true) {
                $_content = @parse_ini_file($file, true);
            } else{
                throw new Exception('authorizer no find') ;
            }
            $authorizer_refresh_token = $_content['authorizer_refresh_token']; 
            $result = $open->refreshAccessToken($authorizer_appid, $authorizer_refresh_token);
            if (empty($result['authorizer_access_token'])) {
                throw new Exception('authorizer_access_token empty') ;
            }
            return $result['authorizer_access_token'];
        };
        
        $app = new \WeOpen\MiniApp($config);
        if (in_array(strtolower($name), ['service', 'miniapp'])) {
            return $app;
        }
        return $app->instance($name, $appid);
    }

    /**
     * 静态初始化对象
     * @param string $name
     * @param array $arguments
     * @return mixed
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public static function __callStatic($name, $arguments)
    {
        return self::instance($name, isset($arguments[0]) ? $arguments[0] : '', self::$type);
    }

    /**
     * 引用命令空间 如需要try catch自行定义
     * use app\common\service\WeTool;
     * 
     * 公共接口 平台公共使用 如:快速注册 扫码绑定 草稿管理等
     * $open =  WeTool::service();
     * $url = $open->getAuthRedirect(base_url());
     * var_dump($url);
     * 
     * 指定接口 必传授权小程序的APPID 如获取小程序基本信息
     * $instance = WeTool::instance('Account', $this->store['wxapp']['app_id']);
     * $rs = $instance->getAccountBasicinfo();
     * var_dump($rs);
    */
}