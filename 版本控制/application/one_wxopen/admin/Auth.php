<?php

namespace app\one_wxopen\admin;

use app\system\admin\Admin;
use app\one_wxopen\service\WeTool;


class Auth extends Admin
{
    public function index()
    {

        $path = env('app_path') . implode(DIRECTORY_SEPARATOR, ['one_wxopen', 'mpdata']) . DIRECTORY_SEPARATOR;
        $_files = $file = glob($path . 'wx????????????????.ini', GLOB_BRACE);
        if ($this->request->isAjax()) {
            $open =  WeTool::service();
            $_action = input('action', '');
            $_authorizer_appid = input('authorizer_appid', '');
            //取详细资料更新
            if ($_action == 'syncinfo') {
                $file = $path . $_authorizer_appid . '.ini';
                $_file = new \one\WriteIniFile($file);
                try {
                    $info = $open->getAuthorizerInfo($_authorizer_appid);
                    $_file->update(['detail' => json_encode($info, 256)])->write();
                } catch (\Exception $e) {
                    return $this->error($e->getMessage());
                }
                return $this->success('获取成功');
            }
            //列表
            if ($_action == 'synclist') {
                try {
                    $list = $open->getAuthorizerList();
                    foreach ($list['list'] as $k => $v) {
                        $app_id = $v['authorizer_appid'];
                        $file = $path . $app_id . '.ini';
                        $_file = new \one\WriteIniFile($file);
                        $_file->update($v)->write();
                        //取详细资料更新
                        $info = $open->getAuthorizerInfo($app_id);
                        $_file->update(['detail'=>json_encode($info,256)])->write();
                    }
                } catch (\Exception $e) {
                    return $this->error($e->getMessage());
                }
                return $this->success('获取成功');
            }
        }
        $_data = [];
        foreach ($file as $k => $v) {
            if (file_exists($v) === true) {
                $_content = @parse_ini_file($v, true, INI_SCANNER_RAW);
                $_content['detail'] = (array)@json_decode($_content['detail'], true);
                $_content = array_merge($_content, $_content['detail']);
                unset($_content['detail']);
                // var_dump(json_decode($_content['detail'],true));
                array_push($_data, $_content);
            }
        }
        return $this->fetch('index', ['table_data' => $_data]);
        // $open =  WeTool::service();
        // $list = $open->getAuthorizerList();
        // var_dump($list);

        // $path = env('app_path').'one_wxopen'.DIRECTORY_SEPARATOR.'mpdata'.DIRECTORY_SEPARATOR;
        // foreach($list['list'] as $k=>$v){
        //     $app_id = $v['authorizer_appid'];
        //     $file = $path.$app_id.'.ini';
        //     $_file = new \one\WriteIniFile($file);
        //     $_file->update($v)->write();
        //     //取详细资料更新
        //     $info = $open->getAuthorizerInfo($app_id);
        //     $_file->update(['detail'=>json_encode($info,256)])->write();
        //     var_dump($info);
        // }
        // die('abc');
        // $file = env('app_path').'one_wxopen'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'wxopen.ini';
        // if (file_exists($file) === true) {
        //     $formData = @parse_ini_file($file, true);
        // } else {
        //     $formData = [];
        // }
        // if ($this->request->isAjax()) {
        //     $_data = input('post.');
        //     unset($_data['_csrf']);
        //     $_file = new \one\WriteIniFile($file);
        //     $_file->update($_data)->write();
        //     return $this->success('更新成功');
        // }
        // $formData['open_auth_domain'] = get_domain();
        // $formData['open_ticket_url'] = get_domain().'/wxopen/ticket';
        // $formData['open_msg_url'] = get_domain().'/wxopen/message/appid/$APPID$';
        // $this->assign(compact('formData'));
        // return $this->fetch();
    }
    public function ext_json_decode($str, $mode = false)
    {
        if (preg_match('/\w:/', $str)) {
            $str = preg_replace('/(\w+[^http:]):(\w+)/is', '"$1":"$2"', $str);
        }
        var_dump($str);
        return json_decode($str, $mode);
    }
}
