<?php

namespace app\upgrade\model;

class UpgradeMd5Log extends Base
{

    public function getInfoAttr($v, $d){
        $info = unserialize($v);
        return "系统：{$info['os']}<br>PHP版本：{$info['php_v']}<br>IP地址：{$info['ip']}";
    }

}
