<template>
    <div class="pages">
        <video
            id="videos"
            ref="videos"
            loop="true"
            :autoplay="autoPlay"
            :src="url"
            :danmu-list="danmuList"
            :enable-danmu="true"
            :danmu-btn="false"
            :page-gesture="true"
            :style="videoStyle"
            :poster="poster"
            :controls="!isLockScreen"
            :enable-play-gesture="true"
            :title="title"
            :object-fit="fill"
            :show-center-play-btn="false"
            @click="videoClick"
            @controlstoggle="controlstoggle"
            @fullscreenchange="setFullScreenStatus"
            @timeupdate="timeUpdate"
            @ended="playNext"
        >
            <!-- 非全屏开始 -->
            <!-- 投屏 -->
            <cover-view class="tv-box" :style="tvBoxStyle" v-if="!isFullScreen && animationMenu && showRight">
                <div><uni-icons class="tv-btn" @click="sendtv" type="toutv" color="#ffffff" size="40"></uni-icons></div>
            </cover-view>
            <cover-view v-if="!isFullScreen && animationMenu && showRight" class="flotageY" :style="flotageY">
                <!-- <div class="right-item"><uni-icons @click="fillTap" :type="fill == 'fill' ? 'fangda' : 'suofang'" color="#ffffff" size="40"></uni-icons></div> -->
                <div @click="playbackRate" class="right-item">
                    <text style="color: #FFFFFF;font-size: 34upx;">x{{ playbackRateList[playbackRateIndex] }}</text>
                </div>
            </cover-view>
            <!-- 快进快推 -->
            <cover-view class="speed" v-if="!isFullScreen && animationMenu && showRight">
                <div class="speed-item speed-item-left"><uni-icons @click="speed('jian')" type="kuaitui" color="#ffffff" size="70"></uni-icons></div>
                <div class="speed-item speed-item-right"><uni-icons @click="speed('jia')" type="kuaijin" color="#ffffff" size="70"></uni-icons></div>
            </cover-view>
            <!-- 跳转至记忆 -->
            <cover-view class="remember-view-box" :style="rememberBoxStyle" v-if="!isFullScreen && initialTime != 0 && remeber && play && !tvMask && !showxx && showRight">
                <div class="remember-view">
                    <div @click="remeber = !remeber" class="btnbox"><uni-icons type="closeempty" color="#ffffff" size="40"></uni-icons></div>
                    <text @click="remeber = !remeber" class="sm-title marginright">{{ i18n.gotoInitialTime }}</text>
                    <div class="btnbox" @click="tapRemeber">
                        <text class="sm-title" :style="{ color }">{{ rememberCurrent }}</text>
                    </div>
                </div>
            </cover-view>
            <!-- 非全屏结束 -->

            <!-- 全屏开始 -->
            <!-- 顶部标题 -->
            <cover-view v-if="isFullScreen && animationMenu && !animation && !isLockScreen" class="full-screen-top-box" :style="titleStyle">
                <div class="full-screen-top-box-left-box">
                    <text class="full-screen-top-box-left-box-right">{{ title }}</text>
                </div>
            </cover-view>
            <!-- 投屏 -->
            <cover-view class="tv-box" :style="tvBoxStyle" v-if="isFullScreen && animationMenu && !animation && !isLockScreen">
                <div><uni-icons @click="sendtv" type="toutv" color="#ffffff" size="40"></uni-icons></div>
            </cover-view>
            <!-- 锁屏 -->
            <cover-view class="lock-box" v-if="isFullScreen && isShowlockButton" :style="lockBoxStyle">
                <div><uni-icons @click="changeScreenLock" :type="!isLockScreen ? 'jiesuo' : 'suoping'" color="#ffffff" size="50"></uni-icons></div>
            </cover-view>
            <!-- 倍速 -->
            <cover-view v-if="animationMenu && !animation && isFullScreen && !isLockScreen" class="flotageYY" :style="floatBoxStyle">
                <div @click="playbackRate" class="right-item">
                    <text style="color: #FFFFFF;font-size: 34upx;margin-top: 20upx;">x{{ playbackRateList[playbackRateIndex] }}</text>
                </div>
            </cover-view>
            <!-- 跳转至记忆 -->
            <cover-view class="remember-view-box" :style="rememberBoxStyle" v-if="isFullScreen && initialTime != 0">
                <div class="remember-view">
                    <div @click="remeber = !remeber" class="btnbox"><uni-icons type="closeempty" color="#ffffff" size="50"></uni-icons></div>
                    <text class="sm-title marginright">{{ i18n.gotoInitialTime }}</text>
                    <div class="btnbox" @click="tapRemeber">
                        <text class="sm-title" :style="{ color }">{{ rememberCurrent }}</text>
                    </div>
                </div>
            </cover-view>
            <!-- 全屏结束 -->

            <!-- 投屏弹层开始 -->
            <cover-view :style="videoStyle" v-if="tvMask" class="tv-mask-box">
                <cover-image :style="tvMaskBgStyle" class="tv-mask-img" :src="tvSendBg"></cover-image>
                <div class="tv-mask" :style="tvMaskBgStyle">
                    <div class="tv-mask-title">
                        <text class="tv-mask-text">{{ tvMaskTitle }}</text>
                    </div>
                    <div class="tv-mask-btn">
                        <div @tap="sendtv">
                            <text class="tv-mask-text">{{ i18n.changeBtn }}</text>
                        </div>
                        <div @tap="exittv">
                            <text class="tv-mask-text">{{ i18n.exitBtn }}</text>
                        </div>
                    </div>
                </div>
            </cover-view>
            <!-- 投屏弹层结束 -->
            <!-- 遮罩开始 -->
            <cover-view :style="videoStyle" v-if="showxx" class="tv-mask-box">
                <cover-image :style="tvMaskBgStyle" class="tv-mask-img" :src="maskBg"></cover-image>
                <div class="tv-mask" :style="tvMaskBgStyle">
                    <div class="tv-mask-title"><text class="tv-mask-text-big">未激活观看权限</text></div>
                    <div class="tv-mask-btn-big">
                        <div @tap="goback" v-if="isFullScreen"><text class="tv-mask-text tv-mask-text-normal">返回</text></div>
                        <div @tap="goPage"><text class="tv-mask-text tv-mask-text-btn">获取权限</text></div>
                    </div>
                </div>
            </cover-view>
            <!-- 遮罩结束 -->
        </video>
        <tvSearch ref="tvSearch" :isShow="tvIsShow" :videoUrls="url"></tvSearch>
    </div>
</template>
<script>
import uniIcons from '@/packageA/components/uni-icons/uni-icons.vue';
import tvSearch from '@/components/tvSearch/tvSearch.nvue';
import Methods from './video-box-ios-methods.js';
export default {
    components: {
        tvSearch,
        uniIcons
    },
    props: {
        color: {
            //主题颜色
            type: String,
            default: '#FF6022'
        },
        autoPlay: {
            type: Boolean,
            default: true
        },
        videoHeight: {
            type: Number,
            default: 0
        },
        showxx: {
            type: Boolean,
            default: false
        },
        url: {
            type: String,
            default: ''
        },
        videoRef: {
            type: String,
            default: ''
        },
        videoStyle: {
            type: String,
            default: ''
        },
        poster: {
            type: String,
            default: ''
        },
        danmuList: {
            type: Array,
            default: []
        },
        showBackBtn: {
            type: Boolean,
            default: false
        },
        showDownloadBtn: {
            type: Boolean,
            default: true
        },
        processBarColor: {
            type: String,
            default: '#39BFFD'
        },
        initialTime: {
            //指定视频初始播放位置，单位为秒（s）。
            type: Number,
            default: 0
        },
        title: {
            type: String,
            default: ''
        }
    },
    ...Methods
};
</script>
<style lang="scss" scoped>
@import './android.scss';
@import './ios.scss';

.pages {
}

.back-button {
    width: 30upx;
    height: 30upx;
    align-items: center;
    justify-content: center;
    opacity: 0.8;
    margin-top: 25upx;
    margin-left: 15upx;
    border-radius: 50%;
    background-color: rgba($color: #000000, $alpha: 0.8);
}

.back-icon {
    width: 20upx;
    height: 20upx;
}

.download-button {
    position: fixed;
    top: 90upx;
    right: 15upx;
    width: 30upx;
    height: 30upx;
    align-items: center;
    justify-content: center;
    opacity: 0.8;
    border-radius: 50%;
    color: #fff;
    background-color: rgba($color: #000000, $alpha: 0.8);
}

.process-text {
    color: #fff;
    font-size: 10px;
}

.video-process-bar {
    height: 2upx;
}

.download-icon-horizontal {
    position: fixed;
    right: 30upx;
    width: 30upx;
    height: 30upx;
    align-items: center;
    justify-content: center;
    opacity: 0.8;
    border-radius: 50%;
    background-color: rgba($color: #000000, $alpha: 0.8);
}

.process-text-horizontal {
    color: #fff;
    font-size: 5upx;
}

.float-rate-box {
}

.rate-nomal-screen {
    position: fixed;
    right: 0;
    border-top-left-radius: 20upx;
    border-bottom-left-radius: 20upx;
    flex-direction: column;
    align-items: center;
    justify-content: space-around;
    background-color: rgba(0, 0, 0, 0.6);
}

.rate-full-screen {
    position: fixed;
    right: 0;
    border-top-left-radius: 20upx;
    border-bottom-left-radius: 20upx;
    flex-direction: column;
    align-items: center;
    justify-content: space-around;
    background-color: rgba(0, 0, 0, 0.6);
}
.rate-box-title-box-title {
    color: #ffffff;
    font-size: 28upx;
    padding: 18upx 0;
    text-align: left;
    justify-content: flex-start;
    align-items: flex-start;
}
.rate-box-item {
    flex-wrap: nowrap;
    flex-direction: row;
    justify-content: space-around;
    align-items: center;
    padding: 18upx 0;
}

.rateBtn {
    position: fixed;
    width: 140upx;
    flex-wrap: nowrap;
    flex-direction: column;
    text-align: center;
    right: 0;
    border-top-left-radius: 20upx;
    border-bottom-left-radius: 20upx;
    align-items: center;
    justify-content: space-around;
    background-color: rgba(0, 0, 0, 0.6);
}

.numb {
    font-size: 30upx;
    color: #fff;
    padding: 15upx;
}

.tv-box {
    position: absolute;
    flex-direction: row;
    justify-content: flex-end;
    top: 30upx;
    right: 30upx;
}

.tv-btn{
    width: 60upx;
    height: 60upx;
}

.lock-box {
    position: absolute;
    border-radius: 50;
    width: 100upx;
    height: 100upx;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    text-align: center;
}

.btnbox {
    padding: 6upx;
}
.marginright {
    margin-right: 10upx;
}
.sm-title {
    font-size: 26upx;
    color: #fff;
}
.remember-view-box {
    position: absolute;
    flex-direction: row;
    justify-content: flex-start;
    align-items: flex-start;
    padding-right: 10upx;
}
.remember-view {
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    background-color: rgba(0, 0, 0, 0.6);
}
.tv-mask-box {
    position: fixed;
}
.tv-mask {
    top: 0;
    left: 0;
    position: fixed;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    font-size: 32upx;
}
.tv-mask-img {
    position: fixed;
    top: 0;
    left: 0;
}
.tv-mask-text {
    color: #ffffff;
    padding: 0 40upx;
}

.tv-mask-btn-big {
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    padding: 72upx 0;
}

.tv-mask-text-normal {
    font-size: 32upx;
    color: #d19a66;
}

.tv-mask-text-btn {
    padding: 14upx 15upx;
    border: 0.5px solid #d19a66;
    border-radius: 10upx;
    font-size: 32upx;
    color: #d19a66;
}

.tv-mask-text-big {
    color: #d19a66;
    font-size: 42upx;
    font-weight: 500;
}

.tv-mask-title {
    font-size: 40upx;
}
.tv-mask-btn {
    flex-direction: row;
    justify-content: space-between;
    padding: 72upx 0;
}
.full-screen-top-box {
    position: absolute;
    left: 0;
    top: 0;
    padding: 20upx 30upx;
    background-color: rgba(0, 0, 0, 0.3);
    transform: translateZ(0);
}
.full-screen-top-box-left-box-right {
    color: #ffffff;
}

/** 自定义图标 **/
.flotage {
    position: fixed;
    left: 50%;
    transform: translateX(50%);
}
.flotageYY {
    position: absolute;
    right: 24upx;
    transform: translateY(20%);
}
.flotageY {
    position: fixed;
    right: 24upx;
    transform: translateY(20%);
}

.load-video-text {
    color: #fff;
    font-size: $uni-font-size-lg;
}

@-webkit-keyframes masked-animation {
    0% {
        background-position: 0 0;
    }

    to {
        background-position: -100% 0;
    }
}
.font-size-uni {
    font-size: $uni-font-size-sm;
}
.speed {
    position: fixed;
    align-items: center;
}
.speed-item {
    width: 50px;
}
.speed-item-left {
    position: fixed;
    left: 50%;
    top: 50%;
    transform: translate(50%, 50%);
}
.speed-item-right {
    position: fixed;
    right: 70%;
    top: 50%;
    transform: translateY(50%);
}
.right-item {
    padding: 10upx 0;
}
</style>
